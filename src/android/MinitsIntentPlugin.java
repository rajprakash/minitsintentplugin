package com.minits.intent;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

public class MinitsIntentPlugin extends CordovaPlugin {
    private static final String TAG = "MinitsIntentPlugin";

    private JSONObject lastEvent;

    private ArrayList<CallbackContext> _handlers = new ArrayList<CallbackContext>();

    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
        Log.d(TAG, "MinitsIntentPlugin: firing up...");

        handleIntent(cordova.getActivity().getIntent());
    }

    @Override
    public void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    public void handleIntent(Intent intent) {

        // read intent
        String action = intent.getAction();
        int flags = intent.getFlags();
        String filePath = null;
        String sendingText = null;

        if ((flags & Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY) == 0) {
            if (intent != null &&  intent.getAction() != null) {
                if (Intent.ACTION_SEND.equals(action)) {
                    boolean error = false;
                    String type = intent.getType();

                    String text = intent.getStringExtra(Intent.EXTRA_TEXT);
                    if (text == null) {
                        CharSequence textSequence = intent.getCharSequenceExtra(Intent.EXTRA_TEXT);
                        if (textSequence != null) {
                            text = textSequence.toString();
                        }
                    }
                    String subject = intent.getStringExtra(Intent.EXTRA_SUBJECT);

                    if (!TextUtils.isEmpty(text)) {
                        if ((text.startsWith("http://") || text.startsWith("https://")) && !TextUtils.isEmpty(subject)) {
                            text = subject + "\n" + text;
                        }
                        sendingText = text;
                    } else if (!TextUtils.isEmpty(subject)) {
                        sendingText = subject;
                    }

//                    Parcelable parcelable = intent.getParcelableExtra(Intent.EXTRA_STREAM);
//                    if (parcelable != null) {
//                        String path;
//                        if (!(parcelable instanceof Uri)) {
//                            parcelable = Uri.parse(parcelable.toString());
//                        }
//                        Uri uri = (Uri) parcelable;
//                        if (uri != null) {
//                            if (AndroidUtilities.isInternalUri(uri, cordova.getActivity().getApplicationContext())) {
//                                error = true;
//                            }
//                        }
//                        if (!error) {
//                                path = AndroidUtilities.getPath(uri, cordova.getActivity().getApplicationContext());
//                                if (path != null) {
//                                    if (path.startsWith("file:")) {
//                                        path = path.replace("file://", "");
//                                    }
//                                    if (type != null ) {
//                                        filePath = path;
//                                    }
//                                }
//
//                        }
//                    } else if (sendingText == null) {
//                        error = true;
//                    }
                    Uri fileUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
                   try {
                       if ("content".equalsIgnoreCase(fileUri.getScheme())) {
//                           ContentResolver cR = this.cordova.getActivity().getApplicationContext().getContentResolver();
//                           Cursor cursor = null;
//                           String[] proj = { MediaStore.Images.Media.DATA };
//                           cursor = cR.query(fileUri,  proj, null, null, null);
//                           int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//                           cursor.moveToFirst();
//                           filePath = cursor.getString(column_index);
                           filePath = AndroidUtilities.getRealPath(fileUri, this.cordova);
                           if(filePath == null) {
                               filePath = fileUri.toString();
                           }
                       } else if ("file".equalsIgnoreCase(fileUri.getScheme())) {
                           filePath = fileUri.getPath();
                       }
                   } catch (Exception e) {
                    //    handleIntentData(e.toString(), filePath, type);
                   }
                    if (error) {
                        Toast.makeText(webView.getContext(), "Unsupported content", Toast.LENGTH_SHORT).show();
                        handleIntentData(sendingText, filePath, type);
                    } else {
                        handleIntentData(sendingText, filePath, type);
                    }
                }
            } else if (Intent.ACTION_SEND_MULTIPLE.equals(action)) {
                boolean error = false;
                try {
                    ArrayList<Parcelable> uris = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
                    String type = intent.getType();
                    if (uris != null) {
                        for (int a = 0; a < uris.size(); a++) {
                            Parcelable parcelable = uris.get(a);
                            if (!(parcelable instanceof Uri)) {
                                parcelable = Uri.parse(parcelable.toString());
                            }
                            Uri uri = (Uri) parcelable;
                            if (uri != null) {

                            }
                        }
                        if (uris.isEmpty()) {
                            uris = null;
                        }
                    }
                    if (uris != null) {
                            for (int a = 0; a < uris.size(); a++) {
                                Parcelable parcelable = uris.get(a);
                                if (!(parcelable instanceof Uri)) {
                                    parcelable = Uri.parse(parcelable.toString());
                                }
                                break;
                            }

                    } else {
                        error = true;
                    }
                } catch (Exception e) {
                    // FileLog.e(e);
                    error = true;
                }
                if (error) {
                    Toast.makeText(webView.getContext(), "Unsupported content", Toast.LENGTH_SHORT).show();
                }
            }
        }


    }

    void handleIntentData(String text, String path, String type) {
        try {
            lastEvent = new JSONObject();
            lastEvent.put("text", text != null ? text.toString() : ' ');
            lastEvent.put("path", path != null ? path.toString() : ' ');
            lastEvent.put("type", type != null ? type.toString() : ' ');
            consumeEvents();
        } catch (Exception ex) {

        }
    }
    public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {
        if(action.equals("onNewIntent")) {
            addHandler(args, callbackContext);
        }
        return true;
    }



    private void consumeEvents() {
        if(this._handlers.size() == 0 || lastEvent == null) {
            return;
        }

        for(CallbackContext callback : this._handlers) {
            sendToJs(lastEvent, callback);
        }
        lastEvent = null;
    }

    private void sendToJs(JSONObject event, CallbackContext callback) {
        final PluginResult result = new PluginResult(PluginResult.Status.OK, event);
        result.setKeepCallback(true);
        callback.sendPluginResult(result);
    }

    private void addHandler(JSONArray args, final CallbackContext callbackContext) {
        this._handlers.add(callbackContext);
        this.consumeEvents();
    }

}
