var exec = require('cordova/exec');

exports.onNewIntent = function (success, error) {
    exec(success, error, 'MinitsIntentPlugin', 'onNewIntent', []);
};
